export MAVEN_VERSION=3.3.1

BASE=/opt

URL=http://192.168.56.1/repo/packages/
# URL=https://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/

echo "Retrieve maven $MAVEN_VERSION binaries"
wget $URL/apache-maven-$MAVEN_VERSION-bin.tar.gz
tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz -C $BASE
ln -s $BASE/apache-maven-$MAVEN_VERSION $BASE/maven
# rm apache-maven-$MAVEN_VERSION-bin.tar.gz

# echo "Update profile with Maven variables"
echo export M2_HOME=$BASE/maven > /etc/profile.d/maven.sh
echo export PATH=$PATH:'$M2_HOME/bin' >> /etc/profile.d/maven.sh

mkdir /home/vagrant/.m2
# settings.xml in repo? 
# wget http://repo_url/settings.xml -O /home/vagrant/.m2/settings.xml

chown -R vagrant:vagrant $BASE/apache-maven-$MAVEN_VERSION $BASE/maven /home/vagrant/.m2
