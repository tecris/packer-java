#!/bin/bash

ECLIPSE_BASE=/opt/eclipse

sudo -u vagrant $ECLIPSE_BASE/eclipse -debug -nosplash \
  -application org.eclipse.equinox.p2.director \
  -repository http://download.eclipse.org/releases/luna/,http://findbugs.cs.umd.edu/eclipse/ \
  -destination $ECLIPSE_BASE \
  -installIU   edu.umd.cs.findbugs.plugin.eclipse.feature.group \
  -profile DefaultProfile -shared
