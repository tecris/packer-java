ECLIPSE_VERSION=jee-luna-SR2

BASE=/opt

URL=http://192.168.56.1/repo/packages/
# URL=http://download.springsource.com/release/ECLIPSE/luna/SR2/

echo "Retrieve Eclipse distribution"
wget $URL/eclipse-$ECLIPSE_VERSION-linux-gtk-x86_64.tar.gz
tar -xvzf eclipse-$ECLIPSE_VERSION-linux-gtk-x86_64.tar.gz -C $BASE
rm eclipse-$ECLIPSE_VERSION-linux-gtk-x86_64.tar.gz
chmod -R +r $BASE/eclipse

echo "Update eclipse.ini"
sed -i -e 's/-XX:MaxPermSize=256m/-XX:MaxPermSize=512m/g' $BASE/eclipse/eclipse.ini
sed -i -e 's/-Xms40m/-Xms1024m/g' $BASE/eclipse/eclipse.ini
sed -i -e 's/-Xmx512m/-Xmx2550m/g' $BASE/eclipse/eclipse.ini

echo "Pause for console output"
sleep 2
