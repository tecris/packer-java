Apply user configuration
===========================================


### Usage

1. Run vagrant (vagrant will run provision shell scripts to add and configure user).
   * `$ vagrant up`
2. Shutdown image. 
3. Package image.(*)
   * `$ vagrant package --output chronos_vXYZ.box`
4. Upload box image to a yet-to-be-defined repository.

In case after shutdown VM has an Aborted state.
 .) Start VM
 .) Power Off (right click on VM -> Close -> Power Off)
 Hopefully VM will show "Powered Off".