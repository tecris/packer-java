#!/bin/bash

# http://download.eclipse.org/tools/gef/gef4/updates/integration/

# org.eclipse.gef4.zest.feature.group, org.eclipse.gef4.zest.sdk.feature.group
# com.vaadin.designer.eclipse.feature.group, com.vaadin.integration.eclipse.feature.group

ECLIPSE_BASE=/opt/eclipse
  
sudo -u vagrant $ECLIPSE_BASE/eclipse -debug -nosplash \
  -application org.eclipse.equinox.p2.director \
  -repository http://download.eclipse.org/releases/luna/,http://download.eclipse.org/tools/gef/gef4/updates/integration/,http://antlreclipse.sourceforge.net/updates/,http://download.eclipse.org/tools/orbit/downloads/drops/R20140525021250/repository/,http://www.apache.org/dist/ant/ivyde/updatesite/ \
  -destination $ECLIPSE_BASE \
  -installIU org.eclipse.gef4.zest.feature.group \
  -installIU org.apache.ivy.feature.feature.group \
  -installIU org.apache.ivy.eclipse.ant.feature.feature.group \
  -installIU org.apache.ivyde.feature.feature.group \
  -installIU org.apache.ivyde.eclipse.resolvevisualizer.feature.feature.group \
  -profile DefaultProfile -shared

