Apply standard packages
===========================================

### This step assumes Ubuntu base installation box is present in vagrant repository.

### Usage

1. Run vagrant (vagrant will run provision shell scripts to add required packages).
   * `$ vagrant up`
2. Package image
   * `$ vagrant package --output ubuntu-14.04.2-desktop-amd64-packed.box`
3. Import generated Box into Vagrant repository(required for next step, user configuration):
   * `$ vagrant box add ubuntu-14.04.2-desktop-amd64-packed ubuntu-14.04.2-desktop-amd64-packed.box`
   

Steps 2 & 3 are only present to brake image creation in multiple steps.
Image creation takes time and if something goes wrong there is no need to start the entire process from start.