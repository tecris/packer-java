Java Development Environment (Ubuntu)
===========================================

Outcome of this project is a Vagrant box based on Ubuntu Desktop LTS 14.04.
Image includes:

| *Tool / App*  | *Version* |
| ------------- | ------------- |
| Oracle JDK | 8|
| Maven | 3.3.1|
| Eclipse | Luna SR2 (4.4.2)|
|Docker|1.6|

# How to build image
## Pre-requisites:
* Machine running Linux (tested with Ubuntu 14.04.3 Desktop x64)
* [Ubuntu 14.04.3 Desktop x64 iso image](http://www.ubuntu.com) v14.04.3
* [Oracle VM VirtualBox](http://www.virtualbox.org) & [Guest Additions](https://www.virtualbox.org/manual/ch04.html) v5.0.14
* [Vagrant](http://www.vagrantup.com) v1.8.1
* [Packer](http://www.packer.io) v0.8.6
* [Ansible](http://docs.ansible.com/intro_installation.html#latest-releases-via-apt-ubuntu) v1.9.0.1

## Steps to build Java development environment (Ubuntu 14.04):
1. [Build](./OS/README.md) Ubuntu base installation vagrant box.
2. [Apply](./packages/README.md) standard packages.
3. [Add and configure user](./user_config/README.md)

# How to use the image
## Pre-requisites:
* [Oracle VM VirtualBox](http://www.virtualbox.org) & [Guest Additions](https://www.virtualbox.org/manual/ch04.html) v5.0.14
* [Vagrant](http://www.vagrantup.com) v1.8.1

## Use image
* In an empty directory run
  * `$ vagrant init chronos_[username]`
* Edit Vagrantfile (generated in previous step)
  * uncomment following lines:
```
      # config.vm.provider "virtualbox" do |vb|
      #   vb.gui = true
      # end
```
* `$ vagrant box add chronos_[username] /path/to/downloaded/image`
* `$ vagrant up`
* Ubuntu will automatically login into user vagrant (password: vagrant)
* Password for custom user: jdev

### Usefull comands
1. Destroy Vagrant box (go back to intial state)
   * `$ vagrant destroy`
2. List vagrant boxes:
   * `$ vagrant box list`
3. Remove box from Vagrant repository:
   * `$ vagrant box remove a_box_name`
