#!/bin/bash

URL=http://172.25.1.130/repo/packages
FIND_BUGS_VERSION=3.0.1.20150306-5afe4d1
  
  echo "Retrieve FindBugs distribution"
wget $URL/edu.umd.cs.findbugs.plugin.eclipse_$FIND_BUGS_VERSION.zip
sudo unzip edu.umd.cs.findbugs.plugin.eclipse_$FIND_BUGS_VERSION.zip -d /opt/eclipse/dropins
