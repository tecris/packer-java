#!/bin/bash

# provision packages
vagrant up

# power off vm (this is needed to execute next step vagrant package ...)
vagrant halt

# package box
vagrant package --output chronos_nova.box
