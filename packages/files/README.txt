About this VM.

# Techno stack:
  .) Java - v1.8.0_45 (default)
  .) Java - v1.7.0_80
    ..) to switch to java 7: sudo update-java-alternatives -s java-7-oracle
    ..) switch back to java 8: sudo update-java-alternatives -s java-8-oracle
  .) Maven - v3.3.1
  .) Eclipse Luna SR2 -v4.4.2
  .) Docker - v1.6
 
# Communication/IM
  .) Jitsi - v2.8

# Misc
 
  .) Run tomcat 7 with docker
    ..) docker pull 172.25.1.130:5000/tomcat7
    ..) $ docker run --name tomcat7 -d -p 8080:8080 -p 23:22 172.25.1.130:5000/tomcat7
	..) - to ssh in container: ssh -p 23 root@localhost (password: root)
	
	
  .) Run mysql 5.6 with docker (for the impatient please pay attention to ALL MYSQL_* variables)
    ..) docker pull 172.25.1.130:5000/mysql56
    ..) $ docker run -d --name mysqldb -e MYSQL_ROOT_PASSWORD=mysql -e MYSQL_DATABASE=hlnetdb -e MYSQL_USER=hlnetuser -e MYSQL_PASSWORD=1hlnetdb! -p 24:22 -p 3306:3306 172.25.1.130:5000/mysql56
	..) - to ssh in container: ssh -p 24 root@localhost (password: root)
	
	
  .) Run servicemix 5.4 with docker
    ..) docker pull 172.25.1.130:5000/servicemix54
    ..) $ docker run --name servicemix54 -d -p 25:22 -p 1099:1099 -p 8101:8101 -p 8181:8181 -p 61616:61616 -p 36888:36888 -p 44444:44444 -v /host/path/deploy:/deploy 172.25.1.130:5000/servicemix54
	..) - to connect to servicemix console: ssh -p 8101 karaf@localhost (password: karaf)
	..) - to ssh in container: ssh -p 25 root@localhost (password: root)


# Docker cheat sheet

docker ps  - list running containers
docker ps -a list running & stoped containers
docker stop container_id - stop container
docker start container_id - start a stoped container
docker rm container_id - delete container

Difference between 'docker run' and 'docker start'?
docker run - spin offs a new container from an image
docker start - starts a stopped container
