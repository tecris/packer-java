#!/bin/bash

# .) create user
# .) copy eclipse desktop shortcut from vagrant to new user
# .) configure user to use docker without sudo
# .) add user to virtual box group

USERNAME=rango
GROUP=dev

sudo useradd -g $GROUP -s /bin/bash -d /home/$USERNAME -m $USERNAME --group sudo

# set password to jdev
echo "$USERNAME:jdev" | sudo chpasswd

# for user to change password with 1st login
sudo chage -d 0 $USERNAME

# lazy way to create eclipse desktop icon
sudo mkdir /home/$USERNAME/Desktop
sudo cp /home/vagrant/Desktop/eclipse.desktop /home/$USERNAME/Desktop/
sudo chown $USERNAME:$GROUP -R /home/$USERNAME/Desktop

# to avoid to use sudo with docker
sudo usermod -aG docker $USERNAME

# required for Virtual Box shared folder
sudo usermod -g vboxsf $USERNAME

# sudo VBoxControl guestproperty enumerate
# sudo VBoxControl guestproperty get /VirtualBox/GuestAdd/SharedFolders/MountPrefix
# sudo VBoxControl guestproperty set /VirtualBox/GuestAdd/SharedFolders/MountPrefix hl
# sudo VBoxControl guestproperty get /VirtualBox/GuestAdd/SharedFolders/MountDir

sudo chown $USERNAME:$GROUP -R /opt/eclipse
